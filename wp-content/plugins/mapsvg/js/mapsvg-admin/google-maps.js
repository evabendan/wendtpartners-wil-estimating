(function($, window){
    var MapSVGAdminGoogleMapsController = function(container, admin, mapsvg){
        this.name = 'google-maps';
        MapSVGAdminController.call(this, container, admin, mapsvg);
    };
    window.MapSVGAdminGoogleMapsController = MapSVGAdminGoogleMapsController;
    MapSVG.extend(MapSVGAdminGoogleMapsController, window.MapSVGAdminController);


    MapSVGAdminGoogleMapsController.prototype.viewLoaded = function(){
        if(!this.mapsvg.getData().mapIsGeo){
            this.view.find('#mapsvg-can-use-gmap').show();
            this.view.find('form').css({
                opacity: 0.4,
                'pointer-events': 'none'
            });
        }
    };
    MapSVGAdminGoogleMapsController.prototype.setEventHandlers = function(){
        var _this = this;

        MapSVG.GoogleMapBadApiKey = function(){
            alert("Google maps API key is incorrect. Change API key, click \"Save\" and RELOAD the page to reload Google maps scripts with correct API key.");
            _this.view.find('#googleMapsOn').prop('checked',false).trigger('change');
        };
        // window.gm_authFailure = function() {
        //     alert("Google maps API key is incorrect");
        //     _data.googleMaps.loaded = false;
        //     document.head.removeChild(_data.googleMapsScript);
        //     _data.googleMapsScript = null;
        //     if(typeof fail == 'function')
        //         fail();
        // };
        //
        var locations = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('formatted_address'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '//maps.googleapis.com/maps/api/geocode/json?address=%QUERY%&sensor=true',
                wildcard: '%QUERY%',
                transform: function(response) {
                    if(response.error_message){
                        alert(response.error_message);
                    }
                    return response.results;
                },
                rateLimitWait: 500
            }
        });
        var thContainer = _this.view.find('#mapsvg-gm-address');
        var tH = thContainer.typeahead(null, {
            name: 'mapsvg-addresses',
            display: 'formatted_address',
            source: locations,
            minLength: 2
        });
        thContainer.on('typeahead:select',function(ev,item){
            var b = item.geometry.bounds ? item.geometry.bounds : item.geometry.viewport;
            var bounds = new google.maps.LatLngBounds(b.southwest, b.northeast);
            _this.mapsvg.getData().googleMaps.map.fitBounds(bounds);
        });

        /*
        this.view.on('click','#mapsvg-google-download',function(e){
            // _this.mapsvg.initGoogleMaps.done(function(googleMaps){
                if(!_this.googleMapsFullscreenWrapper){
                    _this.googleMapsFullscreenWrapper = $(_this.templates.download()).prependTo('body');
                    // _this.googleMapsFullscreen = $('<div id="mapsvg-google-map-fullscreen"></div>').appendTo(_this.googleMapsFullscreenWrapper);
                    // _this.googleMapsFullscreenControls = $('<div id="mapsvg-google-map-fullscreen-controls" class="well">Zoom-in to desired area and click the button:<br /> <a class="btn btn-default" id="mapsvg-gm-download">Download SVG file</a></div>').appendTo(_this.googleMapsFullscreenWrapper);
                    _this.googleMapsFullscreenWrapper.on('click','#mapsvg-gm-download', function(e){
                        e.preventDefault();
                        var link = $(this);
                        var _w = window;

                        // blank space fix
                        var transform=$(".gm-style>div:first>div").css("transform")
                        var comp=transform.split(",") //split up the transform matrix
                        var mapleft=parseFloat(comp[4]) //get left value
                        var maptop=parseFloat(comp[5])  //get top value
                        $(".gm-style>div:first>div").css({ //get the map container. not sure if stable
                            "transform":"none",
                            "left":mapleft,
                            "top":maptop
                        });

                        html2canvas($('#mapsvg-google-map-fullscreen'), {
                            useCORS: true,
                            onrendered: function(canvas) {
                                var dataUrl = canvas.toDataURL("image/png");
                                var bounds = _this.gm.getBounds().toJSON();
                                bounds = [bounds.west, bounds.north, bounds.east, bounds.south];

                                $.post(ajaxurl, {action: 'mapsvg_download_svg', map_id: _this.mapsvg.id, png: dataUrl, bounds: bounds}).done(function(data){
                                    location.href=(data);
                                });
                                // blank space fix back
                                $(".gm-style>div:first>div").css({
                                    left:0,
                                    top:0,
                                    "transform":transform
                                })
                            }
                        });
                    }).on('click','#mapsvg-gm-close', function(){
                        _this.googleMapsFullscreenWrapper.hide();
                    });
                }else{
                    _this.googleMapsFullscreenWrapper.show();
                }

                _this.gm = new google.maps.Map($('#mapsvg-google-map-fullscreen')[0], {
                    zoom: 2,
                    center: new google.maps.LatLng(-34.397, 150.644),
                    mapTypeId: _this.mapsvg.getData().options.googleMaps.type,
                    fullscreenControl: false,
                    // keyboardShortcuts: false,
                    // mapTypeControl: false,
                    // scaleControl: false,
                    // scrollwheel: false,
                    streetViewControl: false
                    // zoomControl: false
                });

            // });
        });
         */

    };

})(jQuery, window);