/**
 * Project filtering form helper
 *
 * @since   1.0.0
 * @package wcci
 */

(function($) {
    $(document).ready(function() {

        var $form = $( '#project-filter' );

        // sanity check
        if ( $form.length < 1 ) return;

        // bind option changes
        var $inputs  = $( 'select, input' , $form );
        var $loading = $( '.loading'      , $form );

        $inputs.on( 'change', submitForm );


        /**
         * Submit the form
         *
         * @since 1.0.0
         */
        function submitForm() {
            $form.submit();
            $loading.fadeToggle();
        }

    }); // document.ready
})(jQuery);

/**
 * Project MapSVG handling
 *
 * NONE OF THIS WILL WORK IF NOT CALLED IN MapSVG MAP'S "JavaScript" SETTINGS!
 *
 * @since   1.0.0
 * @package wcci
 */

/**
 * Handle map init stuff
 *
 * @param {object}  map
 * @since 1.0.0
 */
function wcciProjectMapInit( map ) {

	// go to pre-selected region?
	if ( wcci.location ) {
		var locationID = wcci.location.toUpperCase();
		map.selectRegion( locationID );
		map.centerOn( map.getRegion( locationID ) );
		map.zoom(1);
	}

	// kill loader
	jQuery( '.projects-map-loader' ).fadeOut( 1000 );
}


/**
 * Handle map region click
 *
 * @param {object}  region  clicked region
 * @since 1.0.0
 */
function wcciProjectMapRegionClick( region ) {

	jQuery( '.projects-map-loader' ).fadeIn( 400 );

	var params = {};
	params[ wcci.industryKey ] = wcci.industry;
	params[ wcci.locationKey ] = region.id.toLowerCase();

	window.location = wcci.projectsURL + '?' + jQuery.param( params );
}