/**
 * Project filtering form helper
 *
 * @since   1.0.0
 * @package wcci
 */

(function($) {
    $(document).ready(function() {

        var $form = $( '#project-filter' );

        // sanity check
        if ( $form.length < 1 ) return;

        // bind option changes
        var $inputs  = $( 'select, input' , $form );
        var $loading = $( '.loading'      , $form );

        $inputs.on( 'change', submitForm );


        /**
         * Submit the form
         *
         * @since 1.0.0
         */
        function submitForm() {
            $form.submit();
            $loading.fadeToggle();
        }

    }); // document.ready
})(jQuery);
