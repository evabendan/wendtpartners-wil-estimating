/**
 * Gulp Tasks
 *
 * Make sure browsersync proxy url is set!
 *
 * @since   1.0.0
 * @package wcci
 */

/**
 * SET BROWSERSYNC URL HERE
 */
var bs_url = "http://wcci.test/";

/**
 * Grab gulp packages
 */
var gulp  = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync').create();


/**
 * JShint, concat, and minify head JS
 */
gulp.task('site-js-head', function() {

  return gulp.src([
    'assets/js/head/*.js'
  ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('head.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(sourcemaps.write('.')) // Creates sourcemap for minified JS
    .pipe(gulp.dest('./assets/js'))
});


/**
 * JShint, concat, and minify foot JS
 */
gulp.task('site-js-foot', function() {

  return gulp.src([
    'assets/js/foot/*.js'
  ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('foot.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(sourcemaps.write('.')) // Creates sourcemap for minified JS
    .pipe(gulp.dest('./assets/js'))
});


/**
 * BrowserSync Config
 */
gulp.task('browsersync', function() {

    // Watch files
    var files = [
    	'assets/js/*.js',
    	'**/*.php',
    ];

    browserSync.init(files, {
	    proxy: bs_url,
		  open: false,
    });

    gulp.watch('assets/js/head/*.js', ['site-js-head']).on('change', browserSync.reload);
    gulp.watch('assets/js/foot/*.js', ['site-js-foot']).on('change', browserSync.reload);
});


/**
 * Non-Browsersync JS/SASS watch
 */
gulp.task('watch', function() {

  // Watch js files
  gulp.watch('assets/js/head/*.js', ['site-js-head']);
  gulp.watch('assets/js/foot/*.js', ['site-js-foot']);
});


/**
 * Default: Run js tasks
 */
gulp.task('default', function() {
  gulp.start( 'site-js-head', 'site-js-foot');
});