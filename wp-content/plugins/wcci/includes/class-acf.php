<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Plugin-specific ACF handling
 *
 * @since   1.0.0
 * @package wcci
 */
class WCCI_ACF {

	/**
	 * Local JSON path
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $json_path = WCCI_PLUGIN_DIR . 'acf-json';


	/**
	 * "Name" of new field group that can be moved
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $group_name_key = 'wcci_group_name';


	/**
	 * Field group post ID key
	 *
	 * @var   integer
	 * @since 1.0.0
	 */
	private static $group_id_key = 'wcci_group_id';


	/**
	 * Move field group notice flag key
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $do_notice_key = 'wcci_move_notice';


	/**
	 * Flag for actually moving the field group
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $do_move_key = 'wcci_move_field';


	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'acf/settings/save_json' , array( __CLASS__ , 'save_json'             )        );
		add_action( 'acf/settings/load_json' , array( __CLASS__ , 'load_json'             )        );
		add_action( 'save_post'              , array( __CLASS__ , 'maybe_set_move_notice' ), 10, 3 );
		add_action( 'admin_notices'          , array( __CLASS__ , 'maybe_add_move_notice' )        );
		add_action( 'admin_notices'          , array( __CLASS__ , 'maybe_move_group'      )        );
	}


	/**
	 * Safe field group to plugin if field group already exists here
	 *
	 * @param  string  $path  existing save path
	 * @return string  $path
	 *
	 * @since  1.0.0
	 */
	public static function save_json( $path ) {

		global $post;

		// don't run if no post (like when duplicating)
		if ( empty( $post ) ) {
			return $path;
		}

		// json file name is the field group's post name
		$file_name = $post->post_name . '.json';

		// if the file already exists in the plugin, re-save it here
		if ( file_exists( self::$json_path . "/$file_name" ) ) {
			return self::$json_path;
		}

		return $path;
	}


	/**
	 * Load in local ACF fields
	 *
	 * @param  array  $paths  existing ACF load paths
	 * @return array
	 *
	 * @since  1.0.0
	 */
	public static function load_json( $paths ) {

		$paths[] = self::$json_path;
		return $paths;
	}


	/**
	 * Maybe register an admin notice for moving a newly created field to plugin
	 *
	 * @param array  $post_id  saved ACF post
	 * @since 1.0.0
	 */
	public static function maybe_set_move_notice( $post_id, $post, $update ) {

		// only for field groups and if this is the initialization of the post
		if (
			$post->post_type !== 'acf-field-group' ||
			$post->post_modified_gmt !== $post->post_date_gmt
		) {
			return;
		}

		// add the notice
		add_filter( 'redirect_post_location', array( __CLASS__ , 'add_move_notice_vars' ), 99, 2 );
	}


	/**
	 * Add the field group move notice vars
	 *
	 * @since 1.0.0
	 */
	public static function add_move_notice_vars( $location, $post_id ) {

		remove_filter( 'redirect_post_location', array( __CLASS__, __FUNCTION__ ), 99, 2 );

		$group_post = get_post( $post_id );

		return add_query_arg( array(
			self::$do_notice_key  => true,
			self::$group_name_key => $group_post->post_name,
		), $location );
	}


	/**
	 * Maybe add the actual field group move admin notice
	 *
	 * @since 1.0.0
	 */
	public static function maybe_add_move_notice() {

		// check for flag and group
		$do_notice = wcci_get( self::$do_notice_key );
		$group     = wcci_get( self::$group_name_key );

		if ( wcci_fn_any_empty( $do_notice, $group ) ) {
			return;
		}

		$group = acf_get_field_group( $group );
		if ( empty( $group ) ) return;

		$title    = $group['title'];
		$url      = add_query_arg( 'post_type', 'acf-field-group', admin_url( 'edit.php' ) );
		$move_url = add_query_arg( array(
			self::$group_name_key => $group['key'],
			self::$group_id_key   => $group['ID'],
			self::$do_move_key    => true,
		), $url );

		?>
		<div class="notice notice-warning">

			<h3><?php _e( 'HI ZILLA', 'wcci' ); ?></h3>

			<p><?php printf(
				__( 'It looks like you\'ve added a new ACF field group. If
					"%s" is for a %splugin-related%s entity or feature,
					click the button to move the field\'s JSON to the plugin\'s
					local %sacf-json%s folder for better organization.', 'wcci' ),
				$title, '<b>', '</b>', '<code>', '</code>'
			); ?></p>

			<a href="<?php echo $url; ?>" class="button">
				<?php _e( 'Nah, it\'s theme related', 'wcci' ); ?>
			</a>&nbsp;

			<a href="<?php echo $move_url; ?>" class="button button-primary">
				<?php printf( __( 'Move "%s" To Plugin', 'wcci' ), $title ) ?>
			</a>

			<br><br>
		</div>

		<?php
	}


	/**
	 * Maybe move a field group from the theme to the plugin
	 *
	 * @since 1.0.0
	 */
	public static function maybe_move_group() {

		$do_move    = wcci_get( self::$do_move_key );
		$group_name = wcci_get( self::$group_name_key );
		$group_id   = wcci_get( self::$group_id_key );

		if ( wcci_fn_any_empty( $do_move, $group_name, $group_id ) ) {
			return;
		}

		$title        = get_the_title( $group_id );
		$local_theme  = get_stylesheet_directory() . "/acf-json/$group_name.json";
		$local_plugin = self::$json_path . "/$group_name.json";

		// if it's in the theme, we're good to go
		if ( file_exists( $local_theme ) ) {

			$moved   = rename( $local_theme, $local_plugin );
			$type    = $moved ? 'success' : 'error';
			$message = $moved
				? __( 'The "%s" field group was successfully moved to the plugin and is ready to commit.', 'wcci' )
				: __( 'Although the "%s" field group was found in your theme, there was an error moving it to the plugin.', 'wcci' );

		} elseif ( file_exists( $local_plugin ) ) {

			$type    = 'error';
			$message = __( 'The "%s" field group is already in the plugin.', 'wcci' );

		} else {

			$type    = 'error';
			$message = __( 'The field group could not be found.', 'wcci' );

		}

		printf(
			'<div class="notice notice-%s"><p>%s</p></div>',
			$type, sprintf( $message, $title )
		);
	}
}

new WCCI_ACF;
