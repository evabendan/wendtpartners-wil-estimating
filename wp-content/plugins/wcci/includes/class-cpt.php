<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Register post types
 *
 * @since   1.0.0
 * @package wcci
 */
class WCCI_CPT {

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'init'          , array( __CLASS__ , 'register_cpts' ) );
		add_action( 'wcci/activate' , array( __CLASS__ , 'register_cpts' ) );
	}


	/**
	 * Register the CPTs
	 *
	 * @since 1.0.0
	 */
	public static function register_cpts() {
		register_post_type( WCCI::PROJECT_KEY, self::get_project_args() );
	}


	/**
	 * Get project args
	 *
	 * @return array  $args  project CPT registration args
	 * @since  1.0.0
	 */
	public static function get_project_args() {

		$labels = array(
			'name'                  => _x( 'Projects', 'Post Type General Name', 'wcci' ),
			'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'wcci' ),
			'menu_name'             => __( 'Projects', 'wcci' ),
			'name_admin_bar'        => __( 'Project', 'wcci' ),
			'archives'              => __( 'Project Archives', 'wcci' ),
			'attributes'            => __( 'Project Attributes', 'wcci' ),
			'parent_item_colon'     => __( 'Parent Project:', 'wcci' ),
			'all_items'             => __( 'All Projects', 'wcci' ),
			'add_new_item'          => __( 'Add New Project', 'wcci' ),
			'add_new'               => __( 'Add New', 'wcci' ),
			'new_item'              => __( 'New Project', 'wcci' ),
			'edit_item'             => __( 'Edit Project', 'wcci' ),
			'update_item'           => __( 'Update Project', 'wcci' ),
			'view_item'             => __( 'View Project', 'wcci' ),
			'view_items'            => __( 'View Projects', 'wcci' ),
			'search_items'          => __( 'Search Project', 'wcci' ),
			'not_found'             => __( 'Not found', 'wcci' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'wcci' ),
			'featured_image'        => __( 'Featured Image', 'wcci' ),
			'set_featured_image'    => __( 'Set featured image', 'wcci' ),
			'remove_featured_image' => __( 'Remove featured image', 'wcci' ),
			'use_featured_image'    => __( 'Use as featured image', 'wcci' ),
			'insert_into_item'      => __( 'Insert into project', 'wcci' ),
			'uploaded_to_this_item' => __( 'Upload to this project', 'wcci' ),
			'items_list'            => __( 'Projects list', 'wcci' ),
			'items_list_navigation' => __( 'Projects list navigation', 'wcci' ),
			'filter_items_list'     => __( 'Filter projects list', 'wcci' ),
		);

		$rewrite = array(
			'slug'                  => 'project',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);

		$args = array(
			'label'                 => __( 'Project', 'wcci' ),
			'description'           => __( 'A projects portfolio item', 'wcci' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'thumbnail', 'revisions' ),
			'taxonomies'            => array( 'KEY_FOR_INDUSTRIES', 'KEY_FOR_LOCATIONS' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-hammer',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'projects',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
		);

		return $args;
	}
}

new WCCI_CPT;
