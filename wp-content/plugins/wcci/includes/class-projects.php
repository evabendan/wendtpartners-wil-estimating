<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Handle project archive stuff
 *
 * @since   1.0.0
 * @package wcci
 */
class WCCI_Projects {

	/**
	 * Are we currently on the archive?
	 *
	 * @var   boolean
	 * @since 1.0.0
	 */
	private static $is_archive_query = false;
	private static $is_archive_page  = false;


	/**
	 * Filter values
	 *
	 * @var   mixed
	 * @since 1.0.0
	 */
	private static $industry;
	private static $location;


	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( 'wcci_js_object'       , array( __CLASS__ , 'set_js_object_props' )     );
		add_action( 'init'                 , array( __CLASS__ , 'set_filter_requests' )     );
		add_action( 'pre_get_posts'        , array( __CLASS__ , 'set_is_archive'      )     );
		add_action( 'pre_get_posts'        , array( __CLASS__ , 'set_pagination'      ), 15 );
		add_action( 'wcci_before_posts'    , array( __CLASS__ , 'do_form'             )     );
		add_action( 'after_setup_theme'    , array( __CLASS__ , 'add_options_page'    )     );
		// add_filter( 'wcci_section_post_id' , array( __CLASS__ , 'set_post_id'         )     );
	}


	/**
	 * Set filter requests by $_GET query
	 *
	 * @since 1.0.0
	 */
	public static function set_filter_requests() {
		self::$industry = wcci_get( WCCI::INDUSTRY_KEY );
		self::$location = wcci_get( WCCI::LOCATION_KEY );
	}


	/**
	 * Set archive flag
	 *
	 * @return boolean
	 * @since  1.0.0
	 */
	public static function set_is_archive( $query ) {

		self::$is_archive_query =
			is_post_type_archive( WCCI::PROJECT_KEY )
			&& $query->is_main_query()
			&& ! is_admin();

		if ( self::$is_archive_query ) {
			self::$is_archive_page = true;
		}
	}


	/**
	 * Get archive flag
	 *
	 * @return boolean
	 * @since  1.0.0
	 */
	public static function get_is_archive() {
		return self::$is_archive_page;
	}


	/**
	 * Adjust pagination argument
	 *
	 * @param WP_Query  $query  existing archive query
	 * @since 1.0.0
	 */
	public static function set_pagination( $query ) {

		// sanity check
		if ( ! self::$is_archive_query ) return;

		$query->set( 'posts_per_page', 12 );
	}


	/**
	 * Set wcci JS object props for projects page
	 *
	 * @param  array  existing props
	 * @return array
	 *
	 * @since  1.0.0
	 */
	public static function set_js_object_props( $props ) {

		return array_merge( $props, array(
			'projectsURL' => self::get_url(),
			'industry'    => self::$industry,
			'location'    => self::$location,
		));
	}


	/**
	 * Output the filtering form
	 *
	 * @since 1.0.0
	 */
	public static function do_form() {

		// sanity check
		if ( ! self::$is_archive_page ) return;

		wcci_fn_template_part( 'filter', 'form', [ 'settings' => [
			'project_key'  => WCCI::PROJECT_KEY,
			'industry_key' => WCCI::INDUSTRY_KEY,
			'location_key' => WCCI::LOCATION_KEY,
			'industries'   => WCCI_Taxonomies::get_industries(),
			'locations'    => WCCI_Taxonomies::get_locations(),
			'industry'     => self::$industry,
			'location'     => self::$location,
			'action'       => self::get_url(),
		]]);
	}


	/**
	 * Get project archive URL
	 *
	 * @return string
	 * @since  1.0.0
	 */
	public static function get_url() {
		return get_post_type_archive_link( WCCI::PROJECT_KEY );
	}


	/**
	 * Add the projects page options page
	 *
	 * @since 1.0.0
	 */
	public static function add_options_page() {

		if ( ! function_exists( 'acf_add_options_page' ) ) {
			return;
		}

		acf_add_options_sub_page( array(
			'post_id'		=> WCCI::PROJECT_KEY,
			'page_title'	=> __( 'Projects Page', 'wcci' ),
			'menu_title'	=> __( 'Projects Page', 'wcci' ),
			'parent_slug'	=> 'edit.php?post_type=' . WCCI::PROJECT_KEY,
			'menu_slug'		=> 'page-projects',
		));
	}


	// /**
	//  * Set correct options ID if archive page
	//  *
	//  * This is needed because wcci_section auto-grabs another CPT's (non-
	//  * existant) option if it's the only one with results on the page.
	//  *
	//  * @param  integer  existing post ID
	//  * @return string   options page post ID
	//  *
	//  * @since  1.0.0
	//  */
	// public static function set_post_id( $post_id ) {
	// 	return self::$is_archive_page ? WCCI::PROJECT_KEY : $post_id;
	// }
}

new WCCI_Projects;
