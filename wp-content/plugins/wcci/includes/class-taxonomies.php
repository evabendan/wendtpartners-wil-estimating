<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Register taxonomies
 *
 * @since   1.0.0
 * @package wcci
 */
class WCCI_Taxonomies {

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'init'          , array( __CLASS__ , 'register_taxonomies' ) );
		add_action( 'wcci/activate' , array( __CLASS__ , 'register_taxonomies' ) );
	}


	/**
	 * Register the Taxonomies
	 *
	 * @since 1.0.0
	 */
	public static function register_taxonomies() {
		register_taxonomy( WCCI::INDUSTRY_KEY , WCCI::PROJECT_KEY , self::get_industry_args() );
		register_taxonomy( WCCI::LOCATION_KEY , WCCI::PROJECT_KEY , self::get_location_args() );
	}


	/**
	 * Get industry taxonomy args
	 *
	 * @return array  $args  industry taxonomy registration args
	 * @since  1.0.0
	 */
	public static function get_industry_args() {

		$labels = array(
			'name'                       => _x( 'Industries', 'Taxonomy General Name', 'wcci' ),
			'singular_name'              => _x( 'Industry', 'Taxonomy Singular Name', 'wcci' ),
			'menu_name'                  => __( 'Industries', 'wcci' ),
			'all_items'                  => __( 'All Industries', 'wcci' ),
			'parent_item'                => __( 'Parent Industry', 'wcci' ),
			'parent_item_colon'          => __( 'Parent Industry:', 'wcci' ),
			'new_item_name'              => __( 'New Industry Name', 'wcci' ),
			'add_new_item'               => __( 'Add New Industry', 'wcci' ),
			'edit_item'                  => __( 'Edit Industry', 'wcci' ),
			'update_item'                => __( 'Update Industry', 'wcci' ),
			'view_item'                  => __( 'View Industry', 'wcci' ),
			'separate_items_with_commas' => __( 'Separate industries with commas', 'wcci' ),
			'add_or_remove_items'        => __( 'Add or remove industries', 'wcci' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'wcci' ),
			'popular_items'              => __( 'Popular Industries', 'wcci' ),
			'search_items'               => __( 'Search Industries', 'wcci' ),
			'not_found'                  => __( 'Not Found', 'wcci' ),
			'no_terms'                   => __( 'No industries', 'wcci' ),
			'items_list'                 => __( 'Industries list', 'wcci' ),
			'items_list_navigation'      => __( 'Industries list navigation', 'wcci' ),
		);

		$rewrite = array(
			'slug'                       => 'industry',
			'with_front'                 => true,
			'hierarchical'               => false,
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'rewrite'                    => $rewrite,
		);

		return $args;
	}


	/**
	 * Get location taxonomy args
	 *
	 * @return array  $args  location taxonomy registration args
	 * @since  1.0.0
	 */
	public static function get_location_args() {

		$labels = array(
			'name'                       => _x( 'Locations', 'Taxonomy General Name', 'wcci' ),
			'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'wcci' ),
			'menu_name'                  => __( 'Locations', 'wcci' ),
			'all_items'                  => __( 'All Location', 'wcci' ),
			'parent_item'                => __( 'Parent Location', 'wcci' ),
			'parent_item_colon'          => __( 'Parent Location:', 'wcci' ),
			'new_item_name'              => __( 'New Location Name', 'wcci' ),
			'add_new_item'               => __( 'Add New Location', 'wcci' ),
			'edit_item'                  => __( 'Edit Location', 'wcci' ),
			'update_item'                => __( 'Update Location', 'wcci' ),
			'view_item'                  => __( 'View Location', 'wcci' ),
			'separate_items_with_commas' => __( 'Separate locations with commas', 'wcci' ),
			'add_or_remove_items'        => __( 'Add or remove locations', 'wcci' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'wcci' ),
			'popular_items'              => __( 'Popular Locations', 'wcci' ),
			'search_items'               => __( 'Search Locations', 'wcci' ),
			'not_found'                  => __( 'Not Found', 'wcci' ),
			'no_terms'                   => __( 'No locations', 'wcci' ),
			'items_list'                 => __( 'Locations list', 'wcci' ),
			'items_list_navigation'      => __( 'Locations list navigation', 'wcci' ),
		);

		$rewrite = array(
			'slug'                       => 'location',
			'with_front'                 => true,
			'hierarchical'               => false,
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'rewrite'                    => $rewrite,
		);

		return $args;
	}


	/**
	 * Get industries
	 *
	 * @return array  industry terms
	 * @since  1.0.0
	 */
	public static function get_industries() {
		return get_terms( array( 'taxonomy' => WCCI::INDUSTRY_KEY ) );
	}


	/**
	 * Get locations
	 *
	 * @return array  location terms
	 * @since  1.0.0
	 */
	public static function get_locations() {
		return get_terms( array( 'taxonomy' => WCCI::LOCATION_KEY ) );
	}
}

new WCCI_Taxonomies;
