<?php
/**
 * Misc. helper functions
 *
 * @since   1.0.0
 * @package wcci
 */


/**
 * Are any of these things empty()?
 *
 * Checks all arguments to see if any of them are empty(). Alternative to:
 * if ( empty( this ) || empty( that ) ) {}.
 *
 * @param  mixed
 * @return bool
 *
 * @since  1.0.0
 */
function wcci_fn_any_empty() {

	return ( count(
		array_filter(
			func_get_args(),
			function( $v ){ return empty( $v ); } // wrapper needed for empty()
		)
	) > 0 );
}


/**
 * Returns a $_GET value
 *
 * @param  string  key for $_GET
 * @return mixed   whatever the value is, null if nothing
 *
 * @since 1.0.0
 */
function wcci_get( $key ) {
	return isset( $_GET[ $key ] ) ? sanitize_text_field( $_GET[ $key ] ) : null;
}


/**
 * Wrapper for projects page flag
 *
 * @return boolean
 * @since  1.0.0
 */
function wcci_is_projects() {
	return WCCI_Projects::get_is_archive();
}


/**
 * Project single checker
 *
 * @return boolean
 * @since  1.0.0
 */
function wcci_is_project() {
	return is_singular( WCCI::PROJECT_KEY );
}


/**
 * Get projects page meta fields
 *
 * @return array
 * @since  1.0.0
 */
function wcci_get_projects_meta() {
	return get_fields( WCCI::PROJECT_KEY );
}


/**
 * List out the post's industries
 *
 * @param integer  $post_id
 * @since 1.0.0
 */
function wcci_industry_list( $post_id = null ) {

	$terms = wp_get_object_terms( $post_id ?: get_the_ID(), WCCI::INDUSTRY_KEY );

	foreach ( $terms as $term ) {
		$list[] = $term->name;
	}

	echo is_array( $list ) ? implode( ', ', $list ) : '';
}


/**
 * Get a template part from the plugin
 *
 * @param string  $slug  the slug name for the generic template
 * @param string  $name  the name of the specialized template
 * @param array   $args  arguments passed to wcci_fn_load_template
 *
 * @since 1.0.0
 */
function wcci_fn_template_part( $slug, $name = null, $args = array() ) {

	// allow edge cases to get in here
	do_action( "get_template_part_{$slug}", $slug, $name );

	// build the path
	$path = WCCI_PLUGIN_DIR . '/templates/parts/' . $slug;

	// add specialized name
	$path .= $name ? "-$name" : '';
	$path .= '.php';

	// check for the file
	if ( file_exists( $path ) ) {
		wcci_fn_load_template( $path, false, $args );
	}
}


/**
 * Load template
 *
 * Modified version of WordPress' load_template function
 * with an added argument to pass variables to the required
 * template part to avoid needing memory-hogging global vars.
 *
 * This implementation is similar to what WooCommerce does.
 *
 * @param string  $_template_file  path to the template file
 * @param bool    $require_once    whether to require_once or require
 * @param array   $args            variables extracted to template part
 *
 * @since 1.0.0
 */
function wcci_fn_load_template( $_template_file, $require_once = true, $args = array() ) {

	global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

	if ( is_array( $wp_query->query_vars ) ) {
		extract( $wp_query->query_vars, EXTR_SKIP );
	}

	// modification
	if ( is_array( $args ) ) {
		extract( $args, EXTR_SKIP );
	}

	if ( isset( $s ) ) {
		$s = esc_attr( $s );
	}

	if ( $require_once ) {
		require_once( $_template_file );
	} else {
		require( $_template_file );
	}
}
