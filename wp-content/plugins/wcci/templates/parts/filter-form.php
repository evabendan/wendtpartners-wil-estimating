<?php
/**
 * Project filter form template
 *
 * @since   1.0.0
 * @package blujay
 */

extract( $settings );
?>

<form id="project-filter" action="<?php echo $action; ?>" method="GET">

	<!-- fallback post_type (if permalinks off) -->
	<?php if ( strpos( $action, $project_key ) !== false ) : ?>
		<input type="hidden" name="post_type" value="<?php echo $project_key; ?>">
	<?php endif; ?>


	<!-- industry -->
	<div class="field">
		<label for="industry"><?php _e( 'Industry', 'wcci' ); ?></label>

		<select name="<?php echo $industry_key; ?>">
			<option value=""><?php _e( 'All Industries', 'wcci' ); ?></option>

			<?php foreach ( $industries as $term ) :
				$selected = $industry == $term->slug ? ' selected' : ''; ?>
				<option value="<?php echo $term->slug; ?>"<?php echo $selected; ?>><?php echo $term->name; ?></option>
			<?php endforeach; ?>
		</select>
	</div>


	<!-- location -->
	<div class="field">
		<label for="location"><?php _e( 'Location', 'wcci' ); ?></label>

		<select name="<?php echo $location_key; ?>">
			<option value=""><?php _e( 'All Locations', 'wcci' ); ?></option>

			<?php foreach ( $locations as $term ) :
				$selected = $location == $term->slug ? ' selected' : ''; ?>
				<option value="<?php echo $term->slug; ?>"<?php echo $selected; ?>><?php echo $term->name; ?></option>
			<?php endforeach; ?>
		</select>
	</div>


	<!-- reset -->
	<a href="<?php echo $action; ?>" class="white-bttn"><?php _e( 'Reset', 'wcci' ); ?></a>


	<!-- loading overlay -->
	<!-- <div class="loading"></div> -->

</form>
