<?php
/**
 * WCCI
 *
 * @package     wcci
 * @author      Designzillas
 * @copyright   2018 WCCI
 *
 * @wordpress-plugin
 * Plugin Name: WCCI
 * Plugin URI:  http://www.willisestimating.com/
 * Description: Some custom post type and other non-theme functionality for WCCI.
 * Version:     1.0.0
 * Author:      Designzillas
 * Author URI:  http://www.designzillas.com/
 * Text Domain: wcci
 */

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/**
 * Main class
 *
 * @since 1.0.0
 */
final class WCCI {

	/**
	 * The one and only instance of WCCI
	 *
	 * @var   object
	 * @since 1.0.0
	 */
	private static $instance;


	/**
	 * Plugin version for enqueueing, etc.
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	public $version = '1.1.0';


	/**
	 * CPT Keys
	 *
	 * @since 1.0.0
	 */
	const PROJECT_KEY = 'wcci_project';


	/**
	 * Taxonomy keys
	 *
	 * @since 1.0.0
	 */
	const INDUSTRY_KEY = 'wcci_industry';
	const LOCATION_KEY = 'wcci_location';


	/**
	 * POST/GET action key
	 *
	 * @since 1.0.0
	 */
	const ACTION_KEY = 'wcci_action';


	/**
	 * Main WCCI instance
	 *
	 * @return WCCI
	 * @since  1.0.0
	 */
	public static function instance() {

		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WCCI ) ) {

			self::$instance = new WCCI;
			self::$instance->constants();
			self::$instance->includes();

			add_action( 'init'               , array( self::$instance, 'do_post_actions'  ) );
			add_action( 'wp_enqueue_scripts' , array( self::$instance, 'set_scripts'      ) );
			add_action( 'pre_get_posts'      , array( self::$instance, 'set_search_types' ) );

			register_activation_hook(   WCCI_PLUGIN_FILE, array( self::$instance, 'plugin_activate'   ) );
			register_deactivation_hook( WCCI_PLUGIN_FILE, array( self::$instance, 'plugin_deactivate' ) );

			do_action( 'wcci/loaded' );
		}

		return self::$instance;
	}


	/**
	 * Setup plugin constants
	 *
	 * @since 1.0.0
	 */
	private function constants() {
		define( 'WCCI_VERSION'     , $this->version              );
		define( 'WCCI_PLUGIN_DIR'  , plugin_dir_path( __FILE__ ) );
		define( 'WCCI_PLUGIN_URL'  , plugin_dir_url( __FILE__ )  );
		define( 'WCCI_PLUGIN_FILE' , __FILE__                    );
	}


	/**
	 * Include required files
	 *
	 * @since 1.0.0
	 */
	private function includes() {

		// load every time
		$includes = array(
			'class-cpt',
			'class-taxonomies',
			'class-acf',
			'class-projects',
			'functions',
		);

		// include stuff
		foreach ( $includes as $include ) {
			require_once WCCI_PLUGIN_DIR . 'includes/' . $include . '.php';
		}
	}


	/**
	 * Do plugin activation stuff
	 *
	 * @since 1.0.0
	 */
	public function plugin_activate() {
		do_action( 'wcci/activate' );
		flush_rewrite_rules();
	}


	/**
	 * Do plugin deactivation stuff
	 *
	 * @since 1.0.0
	 */
	public function plugin_deactivate() {
		do_action( 'wcci/deactivate' );
		flush_rewrite_rules();
	}


	/**
	 * Enqueue scripts and stuff
	 *
	 * @since 1.0.0
	 */
	public function set_scripts() {

		$assets = WCCI_PLUGIN_URL . 'assets/';

		wp_enqueue_script(
			'wcci-fn', $assets . 'js/foot.min.js',
			array( 'jquery' ), false, true
		);

		// localized stuff for AJAX and whatnot
		wp_localize_script( 'wcci-fn', 'wcci', apply_filters( 'wcci_js_object', array(
			'industryKey' => self::INDUSTRY_KEY,
			'locationKey' => self::LOCATION_KEY,
		)));
	}


	/**
	 * Set compatible post types in search
	 *
	 * @param WP_Query  $query
	 * @since 1.1.0
	 */
	public function set_search_types( $query ) {

		if ( is_admin() || ! $query->is_main_query() || ! $query->is_search() ) {
			return;
		}

		$query->set( 'post_type', array( 'page', WCCI::PROJECT_KEY, 'team' ) );
	}


	/**
	 * Hooks WCCI actions, when present in the $_POST superglobal. Every
	 * wcci/action present in $_POST is called using do_action.
	 *
	 * @since 1.0.0
	 */
	public function do_post_actions() {

		$key = ! empty( $_POST[ self::ACTION_KEY ] ) ? sanitize_key( $_POST[ self::ACTION_KEY ] ) : false;

		if ( ! empty( $key ) ) {
			do_action( "wcci/{$key}", $_POST );
		}
	}
}


/**
 * The main function that returns WCCI
 *
 * @return object
 * @since  1.0.0
 */
function WCCI() {
	return WCCI::instance();
}

// slaughter
WCCI();
