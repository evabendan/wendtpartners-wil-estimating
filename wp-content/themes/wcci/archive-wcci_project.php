<?php
/**
 * Project archive template
 *
 * @since   1.0.0
 * @package wcci
 */
$meta = function_exists( 'wcci_get_projects_meta' )
	? wcci_get_projects_meta()
	: array();

get_header(); ?>

<div class="page_content projects">

	<!-- map -->
	<?php if ( @$meta['map'] ) : ?>
		<div class="projects-map-wrap">
			<?php echo do_shortcode( '[mapsvg id="' . $meta['map'] . '"]' ) ?>
			<div class="projects-map-loader"><?php _e( 'Loading Map', 'wcci' ); ?></div>
		</div>
	<?php endif; ?>

	<!-- title & filter form -->
	<div class="header-form-wrap">

		<?php if ( @$meta['title'] || @$meta['subtitle'] ) : ?>
			<header>
				<?php echo @$meta['title']    ? "<h2>{$meta['title']}</h2>"    : ''; ?>
				<?php echo @$meta['subtitle'] ? "<h3>{$meta['subtitle']}</h3>" : ''; ?>
			</header>
		<?php endif; ?>

		<?php do_action( 'wcci_before_posts' ); ?>

	</div>


	<!-- projects -->
	<?php if ( have_posts() ) : ?>
		<nav class="project-grid">
			<?php while ( have_posts() ) : the_post(); $post_id = get_the_ID(); ?>

				<a href="<?php the_permalink(); ?>" <?php post_class(); ?>>

					<?php the_post_thumbnail( 'project-page' ); ?>

					<div><div>
						<?php the_title( '<h3>', '</h3>' ); ?>

						<?php if ( $built = get_field( 'built' ) ) : ?>
							<p><?php printf( __( 'Built in %s', 'wcci' ), $built ); ?></p>
						<?php endif; ?>

						<?php if ( $services = get_field( 'services' ) ) : ?>
							<p><?php echo $services; ?></p>
						<?php endif; ?>

						<?php if ( function_exists( 'wcci_industry_list' ) ) : ?>
							<p><?php wcci_industry_list(); ?></p>
						<?php endif; ?>

						<span class="view-project"><?php _e( 'View Project', 'wcci' ); ?></span>
					</div></div>
				</a>
			<?php endwhile; ?>
		</nav>

		<?php html5wp_pagination(); ?>

	<?php else : ?>

		<div class="projects-not-found">
			<h3><?php esc_html_e( 'Sorry, but nothing matched your filter.', 'wcci' ); ?></h3>
		</div>

	<?php endif; ?>

</div><!-- .page_content -->

<?php get_footer();
