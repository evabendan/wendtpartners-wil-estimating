	<!-- Footer section Start -->
	<div id="scroll"></div>
	<footer id="footer">
		<div class="container">
			<div class="col">
				<?php dynamic_sidebar('footer1'); ?>
			</div>

			<div class="col">
				<?php dynamic_sidebar('footer2'); ?>
			</div>

			<div class="col">
				<?php dynamic_sidebar('footer3'); ?>
			</div>

			<div class="col last">
				<?php dynamic_sidebar('footer4'); ?>
			</div>
			
			<div class="associations">
				<?php dynamic_sidebar('footer5'); ?>
			</div>

			<div class="copyright"><?php echo ot_get_option('copy_right_section'); ?></div>
		</div>
	</footer>

<!-- Footer section Start -->

		<?php wp_footer(); ?>
		
		<script>
		$('.navigation').menu({start: 767});
		</script>
		

		</div>
		</div>
	</body>
</html>
