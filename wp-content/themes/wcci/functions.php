<?php
/**
 * WCCI theme functions
 *
 * @package wcci
 */

/**
 * Add theme support
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_image_size( 'home-page', 475, 266, true );
	add_image_size( 'home-page1', 633, 266, true );
	add_image_size( 'about-page', 228, 307, true );
	add_image_size( 'project-page', 396, 279, true );
	add_image_size( 'timeline', 422, 200, true );
}

/**
 * Enqueue scripts
 */
function header_js_scripts() {
    if ( $GLOBALS['pagenow'] != 'wp-login.php' && ! is_admin() ) {
    	wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery.min'      , get_template_directory_uri() . '/js/jquery.min.js'                   );
	    wp_enqueue_script( 'jquery-effects'  , get_template_directory_uri() . '/js/jquery-effects.js'               );
		wp_enqueue_script( 'script'          , get_template_directory_uri() . '/js/script.js'                , true );
		wp_enqueue_script( 'dz'              , get_template_directory_uri() . '/js/dz_patch.js'              , true );
		wp_enqueue_script( 'jquery.mixitup'  , get_template_directory_uri() . '/js/jquery.mixitup.min.js'           );
		wp_enqueue_script( 'main'            , get_template_directory_uri() . '/js/main.js'                  , true );
		wp_enqueue_script( 'modernizr'       , get_template_directory_uri() . '/js/modernizr.js'                    );
		wp_enqueue_script( 'jquery.parallax' , get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js' , true );
		wp_enqueue_script( 'smoothstate'     , get_template_directory_uri() . '/js/jquery.smoothstate.js'    , true );
		wp_enqueue_script( 'transition'      , get_template_directory_uri() . '/js/transition.js'            , true );
		wp_enqueue_script( 'fading-slideshow', get_template_directory_uri() . '/js/fading-slideshow.js'      , true );
		wp_enqueue_script( 'slick-min'       , get_template_directory_uri() . '/js/slick.min.js'             , true );
		wp_enqueue_script( 'slick-init'      , get_template_directory_uri() . '/js/slick-init.js'            , true );
		wp_enqueue_script( 'filters'         , get_template_directory_uri() . '/js/filters1.js'              , array( 'jquery.mixitup' ), true );
		wp_enqueue_script( 'smooth-scroll'   , get_template_directory_uri() . '/js/smooth-scroll.js'         , true );
		wp_enqueue_script( 'timeline'        , get_template_directory_uri() . '/js/timeline.js'              , true );
	}
}

/**
 * Enqueue styles
 */
function site_styles() {
	wp_enqueue_style( 'reset'          , get_template_directory_uri() . '/css/reset.css'            );
	wp_enqueue_style( 'font-awesome'   , get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'main-style'     , get_template_directory_uri() . '/css/style.css'            );
	wp_enqueue_style( 'responsive'     , get_template_directory_uri() . '/css/responsive.css'       );
	wp_enqueue_style( 'style_new'      , get_template_directory_uri() . '/css/style_new.css'        );
	wp_enqueue_style( 'time-line'      , get_template_directory_uri() . '/css/time-line.css'        );
	wp_enqueue_style( 'time-line-caps' , get_template_directory_uri() . '/css/time-line-caps.css'   );
	wp_enqueue_style( 'transition'     , get_template_directory_uri() . '/css/transitions.css'      );
	wp_enqueue_style( 'slick'          , get_template_directory_uri() . '/css/slick.css'            );
	wp_enqueue_style( 'slick-theme'    , get_template_directory_uri() . '/css/slick-theme.css'      );
	wp_enqueue_style( 'timeline'       , get_template_directory_uri() . '/css/timeline.css'         );
	wp_enqueue_style( 'timeline-font'  , get_template_directory_uri() . '/css/font.timeline.css'    );

	if ( class_exists( 'WCCI' ) && ( wcci_is_projects() || wcci_is_project() ) ) {

		$deps = array(
			'main-style', 'style_new', 'responsive', wcci_is_projects() ? 'mapsvg' : null
		);
		wp_enqueue_style( 'wcci-projects', get_template_directory_uri() . '/css/projects.css', array_filter( $deps ) );
	}
}


function register_sitemenu() {
    register_nav_menus( array(
		'primary'     => __( 'primary', 'wcci' ),
		'footer-menu' => __( 'Footer Menu', 'wcci' ),
    ));
}

function my_wp_nav_menu_args( $args = '' ) {
    $args['container'] = false;
    return $args;
}

function my_css_attributes_filter( $var ) {
    return is_array( $var ) ? array() : '';
}

function remove_category_rel_from_category_list( $thelist ) {
    return str_replace( 'rel="category tag"', 'rel="tag"', $thelist );
}

/**
 * Register widget areas
 */
if ( function_exists( 'register_sidebar' ) ) {

	register_sidebar( array(
        'name'          => __( 'Satisfied Clients', 'wcci' ),
        'description'   => __( 'Satisfied clients-area...', 'wcci' ),
        'id'            => 'client',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

	register_sidebar( array(
        'name'          => __( 'Apply Today', 'wcci' ),
        'description'   => __( 'Apply today-area...', 'wcci' ),
        'id'            => 'apply',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 style="display:none;">',
        'after_title'   => '</h2>'
    ));

	register_sidebar( array(
        'name'          => __( 'Contact Us Today', 'wcci' ),
        'description'   => __( 'Contact us today-area...', 'wcci' ),
        'id'            => 'contact',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 style="display:none;">',
        'after_title'   => '</h2>'
    ));

	register_sidebar( array(
        'name'          => __( 'Footer Widget First', 'wcci' ),
        'description'   => __( 'footer widget first-area...', 'wcci' ),
        'id'            => 'footer1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

	register_sidebar( array(
        'name'          => __( 'Footer Widget Second', 'wcci' ),
        'description'   => __( 'footer widget second-area...', 'wcci' ),
        'id'            => 'footer2',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

	register_sidebar( array(
        'name'          => __( 'Footer Widget Third', 'wcci' ),
        'description'   => __( 'footer widget third-area...', 'wcci' ),
        'id'            => 'footer3',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

	register_sidebar( array(
        'name'          => __( 'Footer Widget Fourth', 'wcci' ),
        'description'   => __( 'footer widget fourth-area...', 'wcci' ),
        'id'            => 'footer4',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

    register_sidebar( array(
        'name'          => __( 'Footer Widget Fifth', 'wcci' ),
        'description'   => __( 'footer widget fifth-area...', 'wcci' ),
        'id'            => 'footer5',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

	register_sidebar( array(
        'name'          => __( 'Blog Widget', 'wcci' ),
        'description'   => __( 'blog widget-area...', 'wcci' ),
        'id'            => 'blog-widget',
        'before_widget' => '<div class="widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title deco-headline">',
        'after_title'   => '</h4>'
    ));
}


/* Custom Post Type  */

add_action( 'init', 'codex_book_init' );
function codex_book_init() {
	$labels = array(
		'name'               => _x( 'Teams', 'post type general name', 'wcci' ),
		'singular_name'      => _x( 'Team', 'post type singular name', 'wcci' ),
		'menu_name'          => _x( 'Teams', 'admin menu', 'wcci' ),
		'name_admin_bar'     => _x( 'Team', 'add new on admin bar', 'wcci' ),
		'add_new'            => _x( 'Add New', 'team', 'wcci' ),
		'add_new_item'       => __( 'Add New Team', 'wcci' ),
		'new_item'           => __( 'New Team', 'wcci' ),
		'edit_item'          => __( 'Edit Team', 'wcci' ),
		'view_item'          => __( 'View Team', 'wcci' ),
		'all_items'          => __( 'All Team', 'wcci' ),
		'search_items'       => __( 'Search Teams', 'wcci' ),
		'parent_item_colon'  => __( 'Parent Teams:', 'wcci' ),
		'not_found'          => __( 'No Teams found.', 'wcci' ),
		'not_found_in_trash' => __( 'No Teams found in Trash.', 'wcci' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'team' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'team', $args );
}

function codex_timeline_init() {
	$labels = array(
		'name'               => _x( 'Timeline Events', 'post type general name', 'wcci' ),
		'singular_name'      => _x( 'Timeline Events', 'post type singular name', 'wcci' ),
		'menu_name'          => _x( 'Timeline Events', 'admin menu', 'wcci' ),
		'name_admin_bar'     => _x( 'Timeline Event', 'add new on admin bar', 'wcci' ),
		'add_new'            => _x( 'Add New', 'Timeline Event', 'wcci' ),
		'add_new_item'       => __( 'Add New Timeline Event', 'wcci' ),
		'new_item'           => __( 'New Timeline Event', 'wcci' ),
		'edit_item'          => __( 'Edit Timeline Event', 'wcci' ),
		'view_item'          => __( 'View Timeline Event', 'wcci' ),
		'all_items'          => __( 'All Timeline Events', 'wcci' ),
		'search_items'       => __( 'Search Timeline Events', 'wcci' ),
		'parent_item_colon'  => __( 'Parent Timeline Event:', 'wcci' ),
		'not_found'          => __( 'No Timeline Event found.', 'wcci' ),
		'not_found_in_trash' => __( 'No Timeline Event found in Trash.', 'wcci' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'time_line' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'time_line_event', $args );
}
add_action( 'init', 'codex_timeline_init' );


function codex_project_init() {
	$labels = array(
		'name'               => _x( 'Projects', 'post type general name', 'wcci' ),
		'singular_name'      => _x( 'Project', 'post type singular name', 'wcci' ),
		'menu_name'          => _x( 'Projects', 'admin menu', 'wcci' ),
		'name_admin_bar'     => _x( 'Project', 'add new on admin bar', 'wcci' ),
		'add_new'            => _x( 'Add New', 'project', 'wcci' ),
		'add_new_item'       => __( 'Add New Project', 'wcci' ),
		'new_item'           => __( 'New Project', 'wcci' ),
		'edit_item'          => __( 'Edit Project', 'wcci' ),
		'view_item'          => __( 'View Project', 'wcci' ),
		'all_items'          => __( 'All Project', 'wcci' ),
		'search_items'       => __( 'Search Projects', 'wcci' ),
		'parent_item_colon'  => __( 'Parent Projects:', 'wcci' ),
		'not_found'          => __( 'No Projects found.', 'wcci' ),
		'not_found_in_trash' => __( 'No Projects found in Trash.', 'wcci' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'project' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail', 'excerpt' ),
	);

	register_post_type( 'project', $args );
}
// Disabled for new project CPT in WCCI plugin
// add_action( 'init', 'codex_project_init' );


function add_type_taxonomy() {

	register_taxonomy( 'cpt-type', 'project', array(
		'labels' => array(
			'name'          => _x( 'Types', 'taxonomy general name', 'theme' ),
			'add_new_item'  => __( 'Add New ', 'theme' ),
			'new_item_name' => __( 'New Type', 'theme' ),
		),
		'exclude_from_search' => true,
		'has_archive'         => true,
		'hierarchical'        => true,
		'rewrite'             => array( 'slug' => 'cpt-type', 'with_front' => false ),
		'show_ui'             => true,
		'show_tagcloud'       => false,
	));
}
add_action( 'init', 'add_type_taxonomy' );


function html5wp_pagination() {

    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var( 'paged' )),
        'total' => $wp_query->max_num_pages,
        'show_all'           => false,
		'next_text' => '&raquo',
		'prev_text' => '&laquo',
        'type'=>'list'
    ));
}

function html5wp_blog_pagination() {

    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var( 'paged' )),
        'total' => $wp_query->max_num_pages,
        'show_all'           => false,
		'next_text' => 'Next',
		'prev_text' => 'Previous',
        'type'=>'list'
    ));
}

function html5wp_index( $length ) {
	return 50;
}

function html5wp_custom_post( $length ) {
    return 30;
}

function html5wp_blog( $length ) {
	return 18;
}

function html5wp_excerpt( $length_callback = '', $more_callback = '' ) {

    global $post;
    if ( function_exists( $length_callback ) ) {
        add_filter( 'excerpt_length', $length_callback );
    }
    if ( function_exists( $more_callback ) ) {
        add_filter( 'excerpt_more', $more_callback );
    }
    $output = get_the_excerpt();
    $output = apply_filters( 'wptexturize', $output );
    $output = apply_filters( 'convert_chars', $output );
	$output = wpautop( $output ); // Edited by DZ -- was '<p>' . $output . '</p>';
    echo $output;
}

function html5_blank_view_article( $more ) {
    global $post;
    //return '<a class="view-article" href="' . get_permalink($post->ID) . '">' . __( 'Read More', 'wcci' ) . '</a>';
}

function html5_style_remove( $tag ) {
    return preg_replace( '~\s+type=["\'][^"\']++["\']~', '', $tag );
}

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}


// Add Actions
add_action( 'init', 'header_js_scripts' );
add_action( 'wp_enqueue_scripts', 'site_styles' );
add_action( 'init', 'register_sitemenu' );
add_action( 'init', 'html5wp_pagination' );
//add_action( 'init', 'reg_post_type' );

// Add Filters
add_filter( 'widget_text', 'do_shortcode' ); // Allow shortcodes in Dynamic Sidebar
add_filter( 'widget_text', 'shortcode_unautop' ); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' ); // Remove surrounding <div> from WP Navigation
add_filter( 'the_category', 'remove_category_rel_from_category_list' ); // Remove invalid rel attribute
add_filter( 'the_excerpt', 'shortcode_unautop' ); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter( 'the_excerpt', 'do_shortcode' ); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter( 'excerpt_more', 'html5_blank_view_article' ); // Add 'View Article' button instead of [...] for Excerpts
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to thumbnails
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to post images
remove_filter( 'the_excerpt', 'wpautop' );

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop', 99 );
add_filter( 'the_content', 'shortcode_unautop', 100 );


/**
 * Register shortcodes
 */
function url_shortcode() {
	return get_bloginfo( 'url' );
}

function theme_shortcode_clear( $atts, $content = null ) {
	return '<div class="clearboth"></div>';
}

function theme_shortcode_row( $atts, $content = null ) {
	return '<div class="row">' . do_shortcode( trim( $content ) ) . '</div>';
}

function theme_shortcode_column( $atts, $content = null, $code ) {
	return '<div class="'.$code.'">' . do_shortcode( trim( $content ) ) . '</div>';
}

add_shortcode( 'url', 'url_shortcode' );
add_shortcode( 'clear', 'theme_shortcode_column' );
add_shortcode( 'row', 'theme_shortcode_column' );
add_shortcode( 'one_half', 'theme_shortcode_column' );
add_shortcode( 'one_third', 'theme_shortcode_column' );
add_shortcode( 'one_fourth', 'theme_shortcode_column' );
add_shortcode( 'one_fifth', 'theme_shortcode_column' );
add_shortcode( 'one_sixth', 'theme_shortcode_column' );
add_shortcode( 'one_half_last', 'theme_shortcode_column' );
add_shortcode( 'one_third_last', 'theme_shortcode_column' );
add_shortcode( 'one_fourth_last', 'theme_shortcode_column' );
add_shortcode( 'one_fifth_last', 'theme_shortcode_column' );
add_shortcode( 'one_sixth_last', 'theme_shortcode_column' );


// remember add first parameter post type as 'page'
function my_custom_init() {
	add_post_type_support( 'page', 'page-attributes' );
}
add_action( 'init', 'my_custom_init' );


function killSpaces( $string ) {
    // Lower case everything
    $string = strtolower( $string );
    // Make alphanumeric (removes all other characters)
    $string = preg_replace( "/[^a-z0-9_\s-]/", "", $string );
    // Clean up multiple dashes or whitespaces
    $string = preg_replace( "/[\s-]+/", " ", $string );
    // Convert whitespaces and underscore to dash
    $string = preg_replace( "/[\s_]/", "-", $string );
    return $string;
}


/**
 * Kint debugger (var_dump fallback) wrapper that's easier to find
 *
 * @param mixed  whatever you want
 * @since 1.0.0
 */
function ALAN() {
	call_user_func_array( function_exists( 'd' ) ? 'd' : 'var_dump', func_get_args() );
}


/**
 * Add new ACF WYSIWYG toolbars
 */
function wcci_add_acf_toolbars( $toolbars ) {

	// core "basic" options
	$basic = array(
		'bold', 'italic', 'underline', 'strikethrough',
		'undo', 'redo', 'link', 'unlink',
	);
	$toolbars['Very Basic'][1] = $basic;

	// additional "with headings and lists" options
	array_unshift( $basic, 'formatselect' );
	$basic[] = 'bullist';
	$basic[] = 'numlist';
	$toolbars['Very Basic with Headings and Lists'][1] = $basic;

	return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars', 'wcci_add_acf_toolbars' );


/**
 * Truncate a string
 *
 * @param  string  $text
 * @return string
 *
 * @since  1.1.0
 */
function wcci_get_truncated( $text, $limit = 160 ) {

	return strlen( $text ) <= $limit
		? $text
		: trim( substr( $text, 0, $limit ) ) . '...';
}
