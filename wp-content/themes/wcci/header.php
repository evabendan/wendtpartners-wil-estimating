<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-194x194.png" sizes="194x194">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
		<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>
		<script>
			$(window).load(function() {
			  $("body").removeClass("preload");
			});
		</script>		

	</head>
	<body class="preload">
		
<!-- header start -->
	<header class="row header">
		<div class="top-bar row">
			<div class="container">
				<div class="top-bar-inner">
					<div class="contact-number">
						<a href="tel:<?php echo ot_get_option('phone_number') ?>"><span class="fa fa-phone"></span><span class="number"><?php echo ot_get_option("phone_number") ?></span></a>
					</div>
					<div class="search-bar">
					
					<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
						<input class="search-input" type="search" name="s"   placeholder="<?php _e( 'What are you looking for?', 'wcci' ); ?>">
						<button class="search-submit" type="submit" role="button"><?php _e( 'Search', 'wcci' ); ?></button>
					</form>
					
					</div>
				</div>	
			</div>
		</div>
		
		<div class="lower-header row">
			<div class="container">
				<div class="logo">
					<a href="<?php echo get_site_url(); ?>"><img src="<?php echo ot_get_option("header_logo", "07") ?>" alt=""></a>
				</div>
				<div class="social">
					<ul>

						<?php if ( $linkedin = ot_get_option( 'linkedin' ) ) : ?>
							<li><a href="<?php echo esc_url( $linkedin ); ?>"><i class="fa fa-linkedin"></i></a></li>
						<?php endif; ?>

						<?php if ( $twitter = ot_get_option( 'twitter' ) ) : ?>
							<li><a href="<?php echo esc_url( $twitter ); ?>"><i class="fa fa-twitter"></i></a></li>
						<?php endif; ?>

						<?php if ( $youtube = ot_get_option( 'youtube' ) ) : ?>
							<li><a href="<?php echo esc_url( $youtube ); ?>"><i class="fa fa-youtube-play"></i></a></li>
						<?php endif; ?>

						<?php if ( $instagram = ot_get_option( 'instagram' ) ) : ?>
							<li><a href="<?php echo esc_url( $instagram ); ?>"><i class="fa fa-instagram"></i></a></li>
						<?php endif; ?>

						<?php if ( $email = ot_get_option( 'email' ) ) : ?>
							<li><a href="<?php echo esc_url( $email ); ?>"><i class="fa fa-envelope"></i></a></li>
						<?php endif; ?>

					</ul>
				</div>
				<div class="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'primary')); ?>
				</div>
				
			</div>
		</div>
	</header>
<!-- header end -->

	<div id="main" class="m-scene">

	<div class="scene_element scene_element--fadein">

