<?php 

get_header(); 
$slug = get_page_by_path( 'blog' );

?>

	<main role="main">
		<section class="blog-archive-banner">
			<div class="container">
				<h1 class="common-heading"><?php the_field('heading', $slug->ID); ?></h1>
				<h2 class="common-subheading"><?php the_field('subheading', $slug->ID); ?></h2>
				<div class="blog-archive-content">
					<?php the_field('content', $slug->ID); ?>
				</div>				
			</div>
		</section>
		<!-- section -->
		<section class="blog-archive">						

			<div class="container blog-archive-wrap">
				<?php get_template_part('loop'); ?>
			</div>

			<?php get_template_part('pagination-blog'); ?>
			
		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
