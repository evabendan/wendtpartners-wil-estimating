function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function filterClick(){
	var projectType = getUrlVars()["service"];
	//console.log(typeof(projectType));
	if(projectType === "legal"){
		$( "a.legal" ).trigger( "click" );
		$('.maingallery ul li img').css('width', '100%');
		//$('#Grid').mixitup('filter', '.legal');
		//console.log('legal');

	};
	if(projectType === "estimating"){
		$( "a.estimating" ).trigger( "click" );
		$('.maingallery ul li img').css('width', '100%');
		console.log('this should have worked');
	};
	if(projectType === "insurance"){
		$( "a.insurance" ).trigger( "click" );
		$('.maingallery ul li img').css('width', '100%');
	};
	if(projectType === "mep"){
		$( "a.mep-engineering" ).trigger( "click" );
		$('.maingallery ul li img').css('width', '100%');
	};
}
$(window).load(function(){
	filterClick();
});
