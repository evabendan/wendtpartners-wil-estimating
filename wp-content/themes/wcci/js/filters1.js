function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function serviceClick(){
	var projectType = getUrlVars()["service"];
	$('a.blue-bttn').each(function(){
		var btnSelector = $(this).attr('data-link');

		if(btnSelector === projectType){
			$(this).click();
		}
	})
}
function filterClick(){
	var projectType = getUrlVars()["service"];

	//console.log( 'project type = "'+projectType+'"');

	if(projectType === "legal"){
		$('#Grid').mixItUp({
			load: {
				filter: '.legal'
			}
		});

	};
	if(projectType === "estimating"){
		$('#Grid').mixItUp({
			load: {
				filter: '.estimating'
			}
		});
	};
	if(projectType === "insurance"){
		$('#Grid').mixItUp({
			load: {
				filter: '.insurance'
			}
		});
	};
	if(projectType === "mep"){
		$('#Grid').mixItUp({
			load: {
				filter: '.mep-engineering'
			}
		});
	};
	if(projectType === undefined){
		$('#Grid').mixItUp();
	};

}
$(window).load(function(){
	$('#Grid').on('mixEnd', function(e, state){
	console.log(state.totalShow+' elements match the current filter');
	console.log(state);
});
	filterClick();
	$('.filter-nav li a.filter').click(function(event){
		var selected = $(this).text();
		console.log('selected = ' + selected);
		$('.dropdown-top span').text(selected);
	});





});
var lightboxOnResize = function lightboxOnResize() {
    if ($(window).width() < 480) {
        $('#Grid li.all a')
            .removeProp('rel')
            .addClass('lightboxRemoved');
        $('#Grid li.all a').click(function(event){
          		event.preventDefault();
          	});
    } else {
        $('a.lightboxRemoved').prop('rel', 'lightbox');
    }
}

$(document).ready(lightboxOnResize);
$(window).resize(lightboxOnResize);
