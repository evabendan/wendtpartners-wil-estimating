

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article class="blog-item" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
				<a class="blog-item-img" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail(array(385,220)); // Declare pixel size you need inside the array ?>
				</a>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<div class="blog-item-content">
				<!-- post title -->
				<h2 class="blog-item-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h2>
				<!-- /post title -->

				<!-- post details -->
				<p class="blog-item-date"><?php the_time('F j, Y'); ?></p>
<!-- 				<span class="author"><?php _e( 'Published by', 'wcci' ); ?> <?php the_author_posts_link(); ?></span> -->
				
				<!-- /post details -->
				<div class="blog-item-excerpt">
					<?php html5wp_excerpt('html5wp_blog'); // Build your custom callback length in functions.php ?>
				</div>				

				<a href="<?php the_permalink(); ?>" class="read-more blue-bttn">Read More</a>

				<?php edit_post_link(); ?>
			</div>

		</article>
		<!-- /article -->	

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, nothing to display.', 'wcci' ); ?></h2>
		</article>
		<!-- /article -->

	<?php endif; ?>

