<?php /* Template Name: About Page Template */ get_header(); ?>
<!-- content start -->

	<div class="about-nav-container">
		<div class="container">
			<div class="about-row-nav">													
				<a href="#about-wcci" class="blue-bttn" data-link="about-wcci">Why WCCI</a>
				<a href="#timeline-embed" class="blue-bttn" data-link="timeline-embed">Our Timeline</a>
				<a href="#our-team" class="blue-bttn" data-link="our-team">Our Team</a>					
			</div>
		</div>
	</div>
	
	<section class="content row">
		
		<!-- history section start -->
		<?php if( have_rows('banner_about') ): ?>
					<?php while( have_rows('banner_about') ): the_row(); 
						// vars
							$banner_ab = get_sub_field('banner_ab');
							$text_ab = get_sub_field('text_ab');
							?>	
			<div class="about-banner" id="about-wcci" style="background-image: url(<?php echo $banner_ab; ?>);">
				<div class="container">
					<?php echo $text_ab; ?>
				</div>
			</div>
			<?php endwhile; endif; ?>
		<!-- history section end -->

	
			<div id="timeline-embed" style="width: 100%; height: 600px;"></div>
		
			<div class="our-team row" id="our-team">
				<div class="team-title">
					<h4>Meet The Team</h4>
					<p>Savvy, practiced, and multi-disciplined - we've earned the consultant title</p>
				</div>
				<div class="container">
					<?php $query_args = array( 'post_type' => 'team', 'posts_per_page' => '-1');
						$the_query = new WP_Query( $query_args ); ?>
					<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>
					
					<div class="one-fourth">
						<div class="team-pic">
							<?php 
							$image	=      wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							//the_post_thumbnail(); ?>
							<img src="<?php echo $image; ?>" class="attachment-post-thumbnail wp-post-image" style="width: 100%;" />
							<div class="team-detail">
								<h5><?php the_title(); ?> </h5>
								<span><?php $team_role = get_field('team_role'); echo $team_role; ?></span>
								<p><?php the_excerpt(); ?></p>
								<a href="<?php $linkedin_link = get_field('linkedin_link'); echo $linkedin_link; ?>"><img src="<?php bloginfo('template_url'); ?>/images/l-icon.png" alt=""></a>
							</div>
						</div>
					</div>
					<?php endwhile; endif; wp_reset_query(); ?>
				</div>
			</div>
	</section>

<!-- content end -->

<?php
	//Dev'd by Ben Nacht
	
	//Empty events array
	$events = array(); 
	
	//Query timeline posts
	query_posts('post_type=time_line_event&posts_per_page=-1');
		while(have_posts()) : the_post();
		
		//Setup metadata
		$post_title = get_the_title();
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'timeline', true);
		$thumbnail_url = $thumb_url[0];
		$post_content = get_the_content();
		$timeline_year = get_field('timeline_year');
		
		//Add multidemsional array to empty $events array
		$events[] = array(
			'media' => array("url" => "$thumbnail_url"),
			'start_date' => array("year" => $timeline_year), 
			'text' => array("headline" => $post_title, "text" => $post_content)
		);
		
		endwhile; 
	wp_reset_query();
	
	//Add events array to new events array
	$events = array('events' => $events);
	
	//JSON encode $events array into variable
	$events_json = str_replace('\\/', '/', json_encode($events));
?>

<script type="text/javascript">
	// Print JSON encoded $events array into variable
	var JSONObj = <?php print $events_json; ?>;
  // The TL.Timeline constructor takes at least two arguments:
  // the id of the Timeline container (no '#'), and
  // the URL to your JSON data file or Google spreadsheet.
  // the id must refer to an element "above" this code,
  // and the element must have CSS styling to give it width and height
  // optionally, a third argument with configuration options can be passed.
  // See below for more about options.
  timeline = new TL.Timeline('timeline-embed', JSONObj);
</script>

<?php get_footer(); ?>
