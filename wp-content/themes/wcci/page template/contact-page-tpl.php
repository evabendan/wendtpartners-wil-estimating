<?php /* Template Name: Contact Page Template */ get_header(); ?>
<!-- content start -->

	<!-- Contact Form Start -->

<div class="contactsection">
	<div class="container">
		<div id="contact-form">
			<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						the_content();
					endwhile; 
				endif;
				wp_reset_query();
			?>
		</div>
		
		<div id="contact-us">
			<h3><?php echo get_field('contact_information_header'); ?></h3>
			<h4><?php echo get_field('contact_information_subheader'); ?></h4>
			<div class="contact-information">
				<?php //echo $contact_info; ?>
				<?php if( have_rows('contact_information') ): ?>
					<?php while( have_rows('contact_information') ): the_row(); 
						// vars
						$contact_section_title = get_sub_field('contact_section_title');
						$contact_section_content = get_sub_field('contact_section_content');
					?>	
						<h5><?php echo $contact_section_title; ?></h5>
						<p><?php echo $contact_section_content ?></p>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>		
	</div>
	<?php if( get_field('background_image_title') ): ?>
		<div id="photo-credits">
			<div id="shortcredit">
				<p class="bg-img-title"><?php the_field('background_image_title'); ?></p>
				<p class="bg-img-cred"><?php the_field('background_image_credit'); ?></p>
			 </div>
		</div>
	<?php endif; ?>
</div>

<!-- Contact Form End -->

<!-- Apply  Today Start -->

<div class="apply-today">
	<div class="container">
		<?php dynamic_sidebar('apply'); ?>
	</div>
		
</div>

<!-- Apply  Today End-->
	
<?php get_footer(); ?>
