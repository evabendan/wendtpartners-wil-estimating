<?php /* Template Name: Home Page Template */ get_header(); ?>

	<!-- banner strat -->
	
	<?php if( have_rows('banner') ): ?>
<?php while( have_rows('banner') ): the_row(); 
	// vars
		$banner_image = get_sub_field('banner_image');
		$banner_text = get_sub_field('banner_text');
		?>
<section class="banner row " >
	<div class="banner-inner" style="">
		<div class="container">
			<?php echo $banner_text; ?>
		</div>
	</div>
</section>
<?php endwhile; endif; ?>
<!-- banner end -->



<!-- content start -->

	<section class="content row">
		<!-- one third start -->
			<div class="one-third-outer">
				<div class="container">
					<?php if( have_rows('about_section') ): ?>
					<?php while( have_rows('about_section') ): the_row(); 
						// vars
							$heading = get_sub_field('heading');
							$heading_link = get_sub_field('heading_link');
							$icon_image = get_sub_field('icon_image');
							$content = get_sub_field('content');
							?>
					
					<div class="one-third">
						<div class="icon">
							<a href="<?php echo $heading_link; ?>"><img src="<?php echo $icon_image['url']; ?>" alt=""></a>
						</div>
						<h3><a href="<?php echo $heading_link; ?>"><?php echo $heading; ?></a></h3>
						<?php echo $content; ?>
					</div>
				<?php endwhile; endif; ?>
				</div>
			</div>
		<!-- one third end -->
		
		<!-- experiance section start -->
			<div class="experiance row">
				<div class="container">
					<div class="expriance-inner inner-wrapper">
						<?php $experience = get_field('experience');
							echo $experience;
						?>
						<div class="row">
						<?php if( have_rows('experiance_block_section') ): ?>
							<?php while( have_rows('experiance_block_section') ): the_row(); 
						// vars
							$image_ex = get_sub_field('image_ex');
							$heading_ex = get_sub_field('heading_ex');
							$content_ex = get_sub_field('content_ex');
							$button_link_ex = get_sub_field('button_link_ex');
							$button_text_ex = get_sub_field('button_text_ex');
							?>
						
						<div class="one-half">
							<div class="left-icon">
								<img src="<?php echo $image_ex['url']; ?>" alt="">
							</div>
							<div class="text">
								<h4><?php echo $heading_ex; ?></h4>
								<p><?php echo $content_ex; ?></p>
								<a href="<?php echo $button_link_ex; ?>" class="blue-bttn"><?php echo $button_text_ex; ?></a>
							</div>
						</div>
						<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>
		<!-- experiance section End -->
		
		<!-- Clients section start -->
		
		<div class="clients-section">
			<div class="inner-wrapper">
				
				<?php dynamic_sidebar('client'); ?>
			</div>
		</div>
		
		<!-- Clients section End -->
		
		<!-- Bottom section Start -->
		
		<div class="bottom-section">
		<?php
		$images = array();
		wp_reset_query();
		$query = new wp_Query('post_type=project&posts_per_page=-1&fields=ids');
			if ($query->have_posts()):
			    foreach( $query->posts as $id ):
			        $image = wp_get_attachment_url( get_post_thumbnail_id($id) );
				    //print_r($image);
				    array_push($images, $image);
			    endforeach;
			endif;
		//wp_reset_query(); 
		
		$array1 = array();
		?>
		<?php 
		$image_1  = get_field('image_1');
		$image_2  = get_field('image_2');
		$image_3  = get_field('image_3');
		$image_4  = get_field('image_4');
		$image_5  = get_field('image_5');
		$image_6  = get_field('image_6');
		$image_7  = get_field('image_7');
		$image_8  = get_field('image_8');
		$image_9  = get_field('image_9');
		$image_10  = get_field('image_10');
		$image_11  = get_field('image_11');
		$image_12  = get_field('image_12');
		array_push($array1, $image_1);
		array_push($array1, $image_2);
		array_push($array1, $image_3);
		array_push($array1, $image_4);
		array_push($array1, $image_5);
		array_push($array1, $image_6);
		array_push($array1, $image_7);
		array_push($array1, $image_8);
		array_push($array1, $image_9);
		array_push($array1, $image_10);
		array_push($array1, $image_11);
		array_push($array1, $image_12);
		$image_count = count($array1);
		$dividend = ceil(2);
		$images = array_chunk($images, $dividend);
		?>
		
			<div class="col">
				<div class="img-container">
					<?php $count = 0;
					foreach ($images[0] as $image) {
						//print_r($image);
						if($count === 0){
							echo '<img src="' . $image . '" class="active" alt="" />';
						}
						else{
							echo '<img src="' . $image . '" alt="" />';
						}
		    			
						$count = 1;
					}



					?> 
				</div>
			</div>
			<div class="col big mobile-display">
				<div class="img-container">
					<?php $count = 0;
					foreach ($images[0] as $image) {
						if($count === 0){
							echo '<img src="' . $image . '" class="active" alt="" />';
						}
						else{
							echo '<img src="' . $image . '" alt="" />';
						}
		    			
						$count = 1;
					}



					?> 
				</div>
				<div class="how_can_help">
					<div class="how_can_help_inner">
						<?php $how_can  = get_field('how_can'); 
						echo $how_can;
						?>
					</div>	
				</div>
			</div>
			<div class="col">
				<div class="img-container">
					<?php $count = 0;
					foreach ($images[1] as $image) {
						if($count === 0){
							echo '<img src="' . $image . '" class="active" alt="" />';
						}
						else{
							echo '<img src="' . $image . '" alt="" />';
						}
		    			
						$count = 1;
					}



					?> 
				</div>
			</div>
			<div class="col">
				<div class="img-container">
					<?php $count = 0;
					foreach ($images[2] as $image) {
						if($count === 0){
							echo '<img src="' . $image . '" class="active" alt="" />';
						}
						else{
							echo '<img src="' . $image . '" alt="" />';
						}
		    			
						$count = 1;
					}



					?> 
					</div>
			</div>
			<div class="col big">
				<div class="img-container">					
					<?php $count = 0;
					foreach ($images[3] as $image) {
						if($count === 0){
							echo '<img src="' . $image . '" class="active" alt="" />';
						}
						else{
							echo '<img src="' . $image . '" alt="" />';
						}
		    			
						$count = 1;
					}



					?> 
					</div>
			</div>
			<div class="col">
				<div class="img-container">
					<?php $count = 0;
					foreach ($images[4] as $image) {
						if($count === 0){
							echo '<img src="' . $image . '" class="active" alt="" />';
						}
						else{
							echo '<img src="' . $image . '" alt="" />';
						}
		    			
						$count = 1;
					}



					?> 
				</div>
			</div>
		</div>
		
		<!-- Bottom section End -->
		
	</section>

<!-- content end -->

<?php get_footer(); ?>
