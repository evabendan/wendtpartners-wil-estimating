<?php
/**
 * Projects listing
 *
 * Template Name: Projects
 *
 * @since   1.0.0
 * @package wcci
 */

// projects
$projects = new WP_Query( array(
	'post_type'      => 'project',
	'posts_per_page' => 999999,
));

get_header();
?>

<!-- banner -->
<?php if ( have_rows( 'banner_project' ) ) : while ( have_rows( 'banner_project' ) ) : the_row();

	$banner_image_pro = get_sub_field( 'banner_image_pro' );
	$banner_text_pro  = get_sub_field( 'banner_text_pro' ); ?>

	<section class="services-banner project">
		<div class="container">

			<?php echo $banner_text_pro; ?>

			<!-- <div class="dropdown">
				<div class="dropdown-top">
					<span>All Industries</span>
					<label></label>
				</div>
				<ul class="filter-nav">
					<li><a class="filter" data-filter="all">All Industries</a></li>
					<?php //$allcats = get_categories('taxonomy=cpt-type&type=project');
									//foreach($allcats as $cat) {?>
						<li><a class="filter <?php //echo $cat->slug; ?>" data-filter="<?php //echo $cat->slug; ?>"><?php //echo $cat->name; ?></a></li>

					<?php //} ?>
				</ul>
			</div> -->

		</div><!-- .container -->
	</section>
<?php endwhile; endif; ?>


<!-- map testing -->
<?php echo do_shortcode( '[mapsvg id="597"]' ); ?>


<!-- main project gallery -->
<div class="maingallery">
	<ul id="Grid">
		<?php while ( $projects->have_posts() ) : $projects->the_post();

			$taxonomy_ar = get_the_terms( $post->ID, 'cpt-type' );
			$thumb_id    = get_post_thumbnail_id();
			$thumb_url   = wp_get_attachment_image_src( $thumb_id, '', true );
			$built       = get_field( 'built' );
			$job         = get_field( 'job' ); ?>

			<!-- <li class="mix all <?php //echo $taxonomy_ar[0]->slug; ?>"> -->
			<li class="mix all">
				<!-- <a href="<?php //echo $thumb_url[0]; ?>" rel="lightbox[<?php //echo $taxonomy_ar[0]->slug; ?>]"> -->
				<a href="<?php echo $thumb_url[0]; ?>" rel="lightbox[]">

					<?php the_post_thumbnail( 'project-page' ); ?>

					<div class="filter-content">
						<div class="filter-content-inner">

							<b><?php the_title(); ?></b>
							<?php echo $built ? "<label>$built</label>" : ''; ?>
							<?php echo $job   ? "<label>$job</label>"   : ''; ?>
							<p class="projects-overlay"><?php the_excerpt(); ?></p>

						</div>
					</div>
				</a>
			</li>

		<?php endwhile; ?>
	</ul>
</div><!-- .maingallery -->


<div class="apply-today project">
	<div class="container">
		<?php dynamic_sidebar('contact'); ?>
	</div>
</div>
<?php get_footer(); ?>