<?php /* Template Name: Services Page Template */ get_header(); ?>
<?php

$service = isset( $_GET['service'] ) ? sanitize_text_field( $_GET['service'] ) : '';

if ( $service ) : ?>
	<script type="text/javascript">
		$(document).ready(function(){
			serviceClick(); //Located in Filters1.js
		});
	</script>
<?php endif; ?>

	<div class="services-nav-container">
		<div class="container">
			<div class="services-row-nav">
				<?php if( have_rows('services_list_section') ): ?>
					<?php $row_counter = 1; ?>
					<?php while( have_rows('services_list_section') ): the_row();
						$heading_ser = get_sub_field('heading_ser');
						?>

						<a href="#services_row_<?php echo $row_counter; ?>" class="blue-bttn" data-link="services_row_<?php echo $row_counter; ?>"><?php echo $heading_ser; ?></a>

					<?php $row_counter++; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<!-- banner start -->

	<?php if( have_rows('banner_services') ): ?>
						<?php while( have_rows('banner_services') ): the_row();
							// vars
								$banner_ser = get_sub_field('banner_ser');
								$text_ser = get_sub_field('text_ser');
								?>
	<section class="services-banner" style="background-image: url(<?php echo $banner_ser; ?>);">
		<div class="container">
			<?php echo $text_ser; ?>
			</div>
	</section>
	<?php endwhile; endif; ?>
	<!-- banner end -->

	<!-- Content Start -->

	<div class="services_content">
		<div class="container">
			<?php if( have_rows('services_list_section') ): ?>

				<?php $row_counter = 1; ?>

				<?php while( have_rows('services_list_section') ): the_row();
					// vars
				$heading_ser = get_sub_field('heading_ser');
				$sub_heading_ser = get_sub_field('sub_heading_ser');
				$content_ser = get_sub_field('ser_content');
				$button_text_ser = get_sub_field('button_text_ser');
				$button_link_ser = get_sub_field('button_link_ser');
				$service_icon = get_sub_field('service_icon');
				?>

			<div class="services_content_row" id="services_row_<?php echo $row_counter; ?>" style="background: url(<?php echo $service_icon; ?>) no-repeat;">
				<a id="<?php echo killSpaces($heading_ser); ?>" style="position: relative; top: -50px; display: inline-block;"></a>
				<h4><?php echo $heading_ser; ?></h4>
				<label><?php echo $sub_heading_ser; ?></label>
				<p><?php echo $content_ser; ?></p>
			</div>
			<?php $row_counter++; ?>
	<?php endwhile; endif; ?>


	</div>
</div>

	<div class="workfor" id="workfor">
		<div class="container">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
				the_content();
			endwhile; endif; wp_reset_query(); ?>
		</div>
			<?php if( get_field('background_image_title') ): ?>
				<div id="photo-credits">
					<div id="shortcredit">
						<p class="bg-img-title"><?php the_field('background_image_title'); ?></p>
						<p class="bg-img-cred"><?php the_field('background_image_credit'); ?></p>
					 </div>
				</div>
			<?php endif; ?>
	</div>

	<!-- construction section start -->
	<div class="construction row" id="cost_clarity">
		<div class="container">

			<?php $construction_cost = get_field('construction_cost'); echo $construction_cost;  ?>

			<?php if ( have_rows('construction_cost_list_section') ): ?>
				<?php while ( have_rows('construction_cost_list_section') ): the_row();
					// vars
					$heading_con = get_sub_field('heading_con');
					$content_con = get_sub_field('content_con');
					?>
					<div class="one-half">
						<h3><?php echo $heading_con; ?></h3>
						<p><?php echo $content_con; ?></p>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>

	<!-- construction section end -->
<!-- Content end -->

<?php get_footer();
