<?php get_header(); ?>


	<?php $banner_page = get_field('banner_page'); 
	if( $banner_page ) {
	?>	
	<section class="services-banner" style="background-image: url(<?php echo $banner_page; ?>);">
		<div class="container">
		<h1 class="common-heading">	<?php the_title(); ?></h1>
			</div>
	</section>
	<?php } ?>
	<!-- banner end -->

	<!-- Content Start -->
<div class="page_content">
	<div class="container">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
						<br class="clear">
					
				</article>
			<!-- /article -->
		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'wcci' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

	</div>
</div>
<?php get_footer(); ?>