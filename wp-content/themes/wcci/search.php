<?php get_header(); ?>

<section class="services-banner">
	<div class="container">
		<h1 class="common-heading">	Search </h1>
	</div>
</section>

<div class="page_content page_search">
	<div class="container">

			<h2><?php echo sprintf( __( '%s Search Results for ', 'wcci' ), $wp_query->found_posts ); echo get_search_query(); ?></h2>

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- post thumbnail -->
	
		<!-- /post thumbnail -->

		<!-- post title -->
		<h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		</h2>
		<!-- /post title -->

		<!-- post details -->
		<!-- <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span> -->
		<!-- <span class="author"><?php _e( 'Published by', 'wcci' ); ?> <?php the_author_posts_link(); ?></span> -->
		<!-- <span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'wcci' ), __( '1 Comment', 'wcci' ), __( '% Comments', 'wcci' )); ?></span> -->
		<!-- /post details -->

		<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>

		<?php if ( ! get_the_excerpt() && function_exists( 'get_field' ) ) :
			echo wcci_get_truncated( get_field( 'body' ) );
		endif; ?>

		<?php edit_post_link(); ?>

	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'wcci' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>

			<?php get_template_part('pagination'); ?>
</div>
</div>
	

<?php get_footer(); ?>
