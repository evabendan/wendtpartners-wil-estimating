<?php
/**
 * Project single
 *
 * We can use WCCI:: in here because the template can't really be called
 * without the plugin being on.
 *
 * @since   1.0.0
 * @package wcci
 */

$meta       = get_fields();
$industries = get_the_terms( get_the_ID(), WCCI::INDUSTRY_KEY ) ?: array();

foreach ( $industries as $i => $industry ) {
	$industries[ $i ] = $industry->name;
}

get_header(); ?>

<div class="page_content project">

	<!-- banner -->
	<header>

		<?php the_post_thumbnail( 'full' ); ?>

		<div><div class="container">

			<?php the_title( '<h1>', '</h1>' ); ?>

			<?php echo $industries ? '<h2>' . implode( ', ', $industries ) . '</h2>' : ''; ?>

			<?php if ( $meta['gallery'] ) : ?>
				<a href="<?php echo $meta['gallery'][0]['sizes']['large']; ?>" rel="lightbox[]" class="white-bttn">
					<?php _e( 'View Gallery', 'wcci' ); ?>
				</a>
				<a href="#gallery" class="white-bttn">
					<?php _e( 'View Gallery', 'wcci' ); ?>
				</a>
			<?php endif; ?>
		</div></div>
	</header>


	<div class="container">

		<!-- overview -->
		<article class="entry-content">

			<h2><?php _e( 'Overview', 'wcci' ); ?></h2>
			<?php echo $meta['body']; ?>

			<?php if ( $services = get_field( 'services' ) ) : ?>
				<p><b><?php _e( 'Services Provided', 'wcci' ); ?>:</b> <?php echo $services; ?></p>
			<?php endif; ?>

		</article>

		<!-- details -->
		<aside>

			<h2><?php _e( 'Details', 'wcci' ); ?></h2>

			<dl>
				<?php if ( $meta['location'] ) : ?>
					<dt><?php _e( 'Location', 'wcci' ) ?></dt>
					<dd><?php echo $meta['location'] ?></dd>
				<?php endif; ?>

				<?php if ( $industries ) : ?>
					<dt><?php _e( count( $industries ) > 1 ? 'Industries' : 'Industry' , 'wcci' ); ?></dt>
					<dd class="industries"><ul><li><?php echo implode( '</li><li>', $industries ) ?></li></ul></dd>
				<?php endif; ?>

				<?php if ( $meta['gallery'] ) : ?>
					<dt><?php _e( 'Gallery', 'wcci' ); ?></dt>
					<dd id="gallery" class="gallery"><ul>
						<?php foreach ( $meta['gallery'] as $item ) : ?>

							<li><a href="<?php echo $item['sizes']['large']; ?>" rel="lightbox[]">
								<?php echo wp_get_attachment_image( $item['ID'], 'project-page' ); ?>
								<span><?php _e( 'View Image', 'wcci' );?></span>
							</a></li>

						<?php endforeach; ?>
					</ul></dd>
				<?php endif; ?>
			</dl>

		</aside>

	</div><!-- .container -->
</div><!-- .page_content -->


<div class="apply-today">
	<div class="container">
		<?php dynamic_sidebar('contact'); ?>
	</div>
</div>

<?php get_footer();
