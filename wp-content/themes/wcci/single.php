<?php get_header(); ?>


<!-- Content Start -->
<div class="page_content blog-single-post">
	<div class="container">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="border-holder">
				<!-- post thumbnail -->
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				<?php endif; ?>
				<!-- /post thumbnail -->

				<!-- post title -->
				<h1 class="common-heading blue"><?php the_title(); ?></h1>
				<!-- /post title -->

				
				<!-- /post details -->

				<?php the_content(); // Dynamic Content ?>

				<div class="blog-share">
					<span class="blog-share-title">Share article to:</span>
					<a href="https://www.linkedin.com/shareArticle?mini=true&url='.get_permalink().'&title='.get_the_title().'&summary=&source=" target="_blank"><i class="fa fa-linkedin"></i></a>
					<a href="https://twitter.com/home?status='.get_the_title().' - '.get_permalink().'" target="_blank"><i class="fa fa-twitter"></i></a>
					<a href="https://www.facebook.com/sharer/sharer.php?u='.get_permalink().'" target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="mailto:type email address here?subject=I wanted to share this post with you from <?php bloginfo('name'); ?>&body=<?php the_title('','',true); ?>&#32;&#32;<?php the_permalink(); ?>" title="Email to a friend/colleague" target="_blank"><i class="fa fa-envelope"></i></a>
				</div>
			
			</div>
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'wcci' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>
</div>
</div>

<div class="single-related-posts">
	<?php $orig_post = $post;
	global $post;
	$tags = wp_get_post_tags($post->ID);
	if ($tags) {
		$tag_ids = array();
		foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
		$args=array(
		'tag__in' => $tag_ids,
		'post__not_in' => array($post->ID),
		'posts_per_page'=>3, // Number of related posts that will be shown.
		'ignore_sticky_posts'=>1
		);
		$my_query = new wp_query( $args );
		if( $my_query->have_posts() ) {

			echo '<div id="relatedposts" class="container"><h3 class="related-heading">CHECK OUT THIS OTHER BLOG</h3>';

			while( $my_query->have_posts() ) {
			$my_query->the_post(); ?>

				<!-- article -->
				<article class="blog-item" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
						<!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
							<a class="blog-item-img" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail(array(350,350)); // Declare pixel size you need inside the array ?>
							</a>
						<?php endif; ?>
						<!-- /post thumbnail -->

						<div class="blog-item-content">
							<!-- post title -->
							<h2 class="blog-item-title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
							</h2>
							<!-- /post title -->

							<!-- post details -->
							<p class="blog-item-date"><?php the_time('F j, Y'); ?></p>
							
							<!-- /post details -->
							<div class="blog-item-excerpt">
								<?php html5wp_excerpt('html5wp_blog'); // Build your custom callback length in functions.php ?>
							</div>				

							<a href="<?php the_permalink(); ?>" class="read-more blue-bttn">Read More</a>

							<?php edit_post_link(); ?>
						</div>						
					
				</article>
				<!-- /article -->
			<?php }
			echo '</div>';
		}
	}
	$post = $orig_post;
	wp_reset_query(); ?>
</div>


<?php get_footer(); ?>
